package com.example.fabiendegarne.testtechboxotop.Activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.fabiendegarne.testtechboxotop.Adapters.MovieAdapter;
import com.example.fabiendegarne.testtechboxotop.Models.AppController;
import com.example.fabiendegarne.testtechboxotop.Classes.Movie;
import com.example.fabiendegarne.testtechboxotop.Network.NetworkConfig;
import com.example.fabiendegarne.testtechboxotop.Network.QueryManager;
import com.example.fabiendegarne.testtechboxotop.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ListView listViewMovies;
    private SwipeRefreshLayout movieRefresher;
    private RelativeLayout layoutLoad;

    private ArrayList<Movie> arrayOfMovies;
    private QueryManager queryManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listViewMovies = (ListView) findViewById(R.id.listViewMovies);
        layoutLoad = (RelativeLayout) findViewById(R.id.main_load);
        movieRefresher = (SwipeRefreshLayout) findViewById(R.id.movie_refresher);

        movieRefresher.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                arrayOfMovies = new ArrayList<>();
                queryManager.setPage(0);
                listViewMovies.setAdapter(null);
                searchMovies(queryManager.getSearch());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem item = menu.findItem(R.id.menuSearch);

        SearchView searchView = (SearchView)item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                arrayOfMovies = new ArrayList<>();
                queryManager = new QueryManager();
                searchMovies(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    public void searchMovies(final String searchText){
        layoutLoad.setVisibility(View.VISIBLE);
        String url = NetworkConfig._BASE_URL + queryManager.getQueryUrl(searchText, queryManager.getPage()+1);
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("Search");
                            for(int i=0; i < jsonArray.length(); i++){
                                Movie movie = new Movie(jsonArray.getJSONObject(i));
                                arrayOfMovies.add(movie);
                            }
                            final MovieAdapter movieAdapter = new MovieAdapter(MainActivity.this, arrayOfMovies);
                            if(queryManager.getPage() <= 1) {
                                listViewMovies.setAdapter(movieAdapter);
                                listViewMovies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                        Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
                                        intent.putExtra("movie", arrayOfMovies.get(i));
                                        startActivity(intent);
                                    }
                                });



                                listViewMovies.setOnScrollListener(new AbsListView.OnScrollListener() {
                                    @Override
                                    public void onScrollStateChanged(AbsListView view, int scrollState) {
                                        if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                                                && (listViewMovies.getLastVisiblePosition() - listViewMovies.getHeaderViewsCount() -
                                                listViewMovies.getFooterViewsCount()) >= (movieAdapter.getCount() - 1)) {
                                            searchMovies(queryManager.getSearch());
                                            //layoutLoad.setVisibility(View.VISIBLE);
                                        }
                                    }

                                    @Override
                                    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                                    }
                                });
                            }
                            else{
                                movieAdapter.notifyDataSetChanged();
                            }
                            movieRefresher.setRefreshing(false);
                            layoutLoad.setVisibility(View.GONE);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if(networkResponse != null){
                            //Log.d("codeMain", "code: "+networkResponse.statusCode);
                        }
                        layoutLoad.setVisibility(View.GONE);
                    }
                });
        //Log.d("request", "request: "+jsonObjReq.toString());
        AppController.getInstance().addToRequestQueue(jsonObjReq, "movies");
    }

    @Override
    public void onBackPressed() {
    }
}
