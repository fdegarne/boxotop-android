package com.example.fabiendegarne.testtechboxotop.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.example.fabiendegarne.testtechboxotop.Classes.Movie;
import com.example.fabiendegarne.testtechboxotop.R;

import java.util.List;

public class MovieAdapter extends ArrayAdapter<Movie> {
    private Context context;
    public MovieAdapter(@NonNull Context context, List<Movie> movies) {
        super(context, 0, movies);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.movie_adapter, parent, false);
            MovieViewHolder viewHolder = new MovieViewHolder();
            viewHolder.image = (NetworkImageView) convertView.findViewById(R.id.movie_image);
            viewHolder.title = (TextView) convertView.findViewById(R.id.title_movie);
            viewHolder.year = (TextView) convertView.findViewById(R.id.year_movie);
            convertView.setTag(viewHolder);
        }

        MovieViewHolder viewHolder = (MovieViewHolder) convertView.getTag();
        Movie movie = getItem(position);

        ImageLoader imageLoader = new ImageLoader(Volley.newRequestQueue(this.context), new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);

            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }

            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
        });
        viewHolder.image.setImageUrl(movie.getPoster(), imageLoader);
        viewHolder.title.setText(movie.getTitle());
        viewHolder.year.setText(String.valueOf(movie.getYear()));


        if(position%2 == 1)
            convertView.setBackgroundColor(convertView.getResources().getColor(R.color.boxotopGreenLight));
        else
            convertView.setBackgroundColor(convertView.getResources().getColor(R.color.boxotopLightGray));
        return convertView;
    }

    private class MovieViewHolder{
        public NetworkImageView image;
        public TextView title;
        public TextView year;
    }
}
