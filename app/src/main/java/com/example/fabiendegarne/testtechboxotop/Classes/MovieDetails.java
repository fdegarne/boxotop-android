package com.example.fabiendegarne.testtechboxotop.Classes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MovieDetails extends Movie {
    private String release;
    private String runtime;
    private String director;
    private String synopsis;
    private float imdbRating;
    private String language;

    private String writers;
    private String casting;

    public MovieDetails(String title, int year, String imdbID, String poster, String release, String runtime, String director, String synopsis, float imdbRating, String language, String writers, String casting) {
        super(title, year, imdbID, poster);
        this.release = release;
        this.runtime = runtime;
        this.director = director;
        this.synopsis = synopsis;
        this.imdbRating = imdbRating;
        this.language = language;
        this.writers = writers;
        this.casting = casting;
    }

    public MovieDetails(JSONObject object) throws JSONException {
        super(object);
        this.release = object.has("Released") ? object.getString("Released") : " ";
        this.runtime = object.has("Runtime") ? object.getString("Runtime") : " ";
        this.director = object.has("Director") ? object.getString("Director") : " ";
        this.synopsis = object.has("Plot") ? object.getString("Plot") : " ";
        this.imdbRating = rateToFloat(object.has("imdbRating") ? object.getString("imdbRating") : " ");
        this.language = object.has("Language") ? object.getString("Language") : " ";
        this.writers = object.has("Writer") ? object.getString("Writer") : " ";
        this.casting = object.has("Actors") ? object.getString("Actors") : " ";
    }

    private float rateToFloat(String rate){
        if (rate.trim().isEmpty()){
            return 0.0f;
        }
        else{
            return Float.valueOf(rate)/2;
        }
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public float getImdbRating() {
        return imdbRating;
    }

    public void setImdbRating(float imdbRating) {
        this.imdbRating = imdbRating;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getWriters() {
        return writers;
    }

    public void setWriters(String writers) {
        this.writers = writers;
    }

    public String getCasting() {
        return casting;
    }

    public void setCasting(String casting) {
        this.casting = casting;
    }
}
