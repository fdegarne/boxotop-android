package com.example.fabiendegarne.testtechboxotop.Classes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class Movie implements Serializable {
    private String title;
    private int year;
    private String imdbID;
    private String poster;

    public Movie(String title, int year, String imdbID, String poster) {
        this.title = title;
        this.year = year;
        this.imdbID = imdbID;
        this.poster = poster;
    }

    public Movie(JSONObject object) throws JSONException {
        this.imdbID = object.has("imdbID") ? object.getString("imdbID") : " ";
        this.title = object.has("Title") ? object.getString("Title") : " ";
        this.poster = object.has("Poster") ? object.getString("Poster") : " ";
        this.year = object.getInt("Year");
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getImdbID() {
        return imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }
}
