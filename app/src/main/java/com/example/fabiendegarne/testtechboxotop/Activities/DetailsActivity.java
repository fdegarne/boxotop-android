package com.example.fabiendegarne.testtechboxotop.Activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.LruCache;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.example.fabiendegarne.testtechboxotop.Adapters.MovieAdapter;
import com.example.fabiendegarne.testtechboxotop.Classes.Movie;
import com.example.fabiendegarne.testtechboxotop.Classes.MovieDetails;
import com.example.fabiendegarne.testtechboxotop.Models.AppController;
import com.example.fabiendegarne.testtechboxotop.Network.NetworkConfig;
import com.example.fabiendegarne.testtechboxotop.Network.QueryManager;
import com.example.fabiendegarne.testtechboxotop.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DetailsActivity extends AppCompatActivity {
    private TextView titleTextView;
    private TextView releaseTextView;
    private TextView synopsisTextView;
    private TextView castingTextView;
    private TextView directionTextView;
    private TextView writerTextView;
    private TextView runtimeTextView;
    private TextView languageTextView;

    private NetworkImageView movieImageView;

    private RatingBar audienceRatingBar;

    private QueryManager queryManager;
    private MovieDetails movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        titleTextView = (TextView) findViewById(R.id.detail_title);
        releaseTextView = (TextView) findViewById(R.id.release_value);
        synopsisTextView = (TextView) findViewById(R.id.synopsis_value);
        castingTextView = (TextView) findViewById(R.id.casting_value);
        directionTextView = (TextView) findViewById(R.id.direction_value);
        writerTextView = (TextView) findViewById(R.id.writer_value);
        runtimeTextView = (TextView) findViewById(R.id.runtime_value);

        movieImageView = (NetworkImageView) findViewById(R.id.detail_image);

        languageTextView = (TextView) findViewById(R.id.language_value);
        audienceRatingBar = (RatingBar) findViewById(R.id.rating_audience);

        Movie movieShort = (Movie)getIntent().getSerializableExtra("movie");
        queryManager = new QueryManager(movieShort.getImdbID());

        titleTextView.setText(movieShort.getTitle());
        ImageLoader imageLoader = new ImageLoader(Volley.newRequestQueue(this), new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);

            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }

            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
        });
        movieImageView.setImageUrl(movieShort.getPoster(), imageLoader);

        loadMovieDetails(movieShort.getImdbID());
    }

    private void loadMovieDetails(String imdbID){
        String url = NetworkConfig._BASE_URL + queryManager.getDetailsUrl();
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            movie = new MovieDetails(response);
                            fillDetails();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if(networkResponse != null){
                            //Log.d("codeMain", "code: "+networkResponse.statusCode);
                        }
                    }
                });
        //Log.d("request", "request: "+jsonObjReq.toString());
        AppController.getInstance().addToRequestQueue(jsonObjReq, "details");
    }

    private void fillDetails(){
        releaseTextView.setText(movie.getRelease());
        runtimeTextView.setText(movie.getRuntime());
        languageTextView.setText(movie.getLanguage());

        audienceRatingBar.setRating(movie.getImdbRating());

        synopsisTextView.setText(movie.getSynopsis());
        castingTextView.setText(movie.getCasting());
        directionTextView.setText(movie.getDirector());
        writerTextView.setText(movie.getWriters());
    }
}
