package com.example.fabiendegarne.testtechboxotop.Network;

public class QueryManager {
    private String search;
    private String type;
    private int page;
    private long totalResults;
    private String imdbID;

    public QueryManager(){
        this.search = "";
        this.page = 0;
        this.totalResults = 0;
        this.type = NetworkConfig._DEFAULT_TYPE;
    }

    public QueryManager(String search, int page, long totalResults, String type) {
        this.search = search;
        this.page = page;
        this.totalResults = totalResults;
        this.type = type;
    }

    public QueryManager(String imdbId){
        this.imdbID = imdbId;
    }

    public String getQueryUrl(String search, int page){
        this.search = search;
        this.page = page;
        return "&s=" + search + "&type=" + type + "&page=" + page;
    }

    public String getDetailsUrl(){
        return "&i=" + imdbID;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public long getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(long totalResults) {
        this.totalResults = totalResults;
    }
}
