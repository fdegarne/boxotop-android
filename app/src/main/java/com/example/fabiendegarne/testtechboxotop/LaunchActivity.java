package com.example.fabiendegarne.testtechboxotop;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

import com.example.fabiendegarne.testtechboxotop.Activities.MainActivity;

public class LaunchActivity extends Activity {

    private TextView titleText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        titleText = (TextView) findViewById(R.id.textTitle);

        AnimationSet setTitleAnimation = new AnimationSet(true);

        final TranslateAnimation translate = new TranslateAnimation(0, 0, -50, 0);
        translate.setDuration(1000);
        translate.setFillEnabled(true);
        translate.setFillAfter(true);
        setTitleAnimation.addAnimation(translate);

        final Animation fadeIn = new AlphaAnimation(0.0f, 1.0f);
        fadeIn.setDuration(1000);
        setTitleAnimation.addAnimation(fadeIn);

        setTitleAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent i = new Intent(LaunchActivity.this, MainActivity.class);
                        LaunchActivity.this.startActivity(i);
                    }
                }, 1000);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        titleText.startAnimation(setTitleAnimation);

    }
}
