package com.example.fabiendegarne.testtechboxotop.Network;

import java.io.StringBufferInputStream;

public class NetworkConfig {

    public static String _SCHEME = "http";
    public static String _HOST = "www.omdbapi.com";
    public static String _APIKEY = "4f209d26";

    public static String _BASE_URL = _SCHEME + "://"+_HOST+"/?apikey="+_APIKEY;

    public static String _DEFAULT_TYPE = "movie";
}
